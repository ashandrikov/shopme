$(document).ready(function () {
    $(".link-delete").on("click", function (e) {
        e.preventDefault();
        userId = $(this).attr("userId");
        $("#yesButton").attr("href", $(this).attr("href"));
        $("#confirmText").text("Are you sure you want to delete this user ID '" + userId + "'?");
        $("#confirmModal").modal();
    });
});

function clearFilter() {
    window.location = "/shopmeadmin/users";
}