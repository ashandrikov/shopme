function checkPasswordMatch(confirmPassword){
    if (confirmPassword.value != $('#password').val()) {
        confirmPassword.setCustomValidity("Passwords don't match!");
    } else {
        confirmPassword.setCustomValidity("");
    }
}