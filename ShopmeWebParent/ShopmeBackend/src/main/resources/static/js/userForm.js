function checkEmailUnique(form) {
    url = "/shopmeadmin/users/check_email";
    userId = $("#id").val();
    userEmail = $("#email").val();
    csrfValue = $("input[name='_csrf']").val();

    params = {id: userId, email: userEmail, _csrf: csrfValue};

    $.post(url, params, function (response) {
        console.log(response);
        if (response == "OK") {
            form.submit();
        } else if (response == "Duplicated") {
            showModalDialog("Warning", "There is another user having the email: " + userEmail);
        } else {
            showModalDialog("Error", "Unknown response from server");
        }
    }).fail(function () {
        showModalDialog("Error", "Could not connect to the server");
    });
    return false;
}

function showModalDialog(title, message) {
    $("#modalTitle").text(title);
    $("#modalBody").text(message);
    $("#modalDialog").modal();
}