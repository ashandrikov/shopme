package com.shopme.admin.user.controller;

import com.shopme.admin.user.UserService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
public class UserRestController {

    private UserService userService;

    @PostMapping("/users/check_email")
    public String checkDuplicateEmail(Integer id, String email) {
        return userService.isEmailUnique(id, email) ? "OK" : "Duplicated";
    }
}
