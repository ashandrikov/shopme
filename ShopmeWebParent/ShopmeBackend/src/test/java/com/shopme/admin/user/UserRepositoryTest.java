package com.shopme.admin.user;

import com.shopme.common.entity.Role;
import com.shopme.common.entity.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.annotation.Rollback;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest(showSql = false)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Rollback(value = false)
public class UserRepositoryTest {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private TestEntityManager entityManager;

    @Test
    public void createNewUserWithOneRoleTest(){
        Role roleAdmin = entityManager.find(Role.class, 1);
        User userAshandri = new User("ashandri@gmail.com", "password", "Tom", "Shandrikov");
        userAshandri.addRole(roleAdmin);

        User savedUser = userRepository.save(userAshandri);

        assertThat(savedUser.getId()).isGreaterThan(0);
    }

    @Test
    public void createNewUserWithTwoRolesTest(){
        Role roleEditor = entityManager.find(Role.class, 3);
        Role roleAssistant = entityManager.find(Role.class, 5);
        User userRavi = new User("ravi@gmail.com", "password", "Ravi", "Kumar");
        userRavi.addRole(roleEditor);
        userRavi.addRole(roleAssistant);

        User savedUser = userRepository.save(userRavi);

        assertThat(savedUser.getId()).isGreaterThan(0);
    }

    @Test
    public void getAllUsersTest(){
        Iterable<User> listUsers = userRepository.findAll();
        listUsers.forEach(user -> System.out.println(user));
    }

    @Test
    public void getUserByIdTest(){
        User userAshan = userRepository.findById(1).get();
        assertThat(userAshan).isNotNull();
    }

    @Test
    public void updateUserDetailsTest(){
        User userAshan = userRepository.findById(1).get();
        userAshan.setEnabled(true);
        userAshan.setEmail("ashandri2@gmail.com");

        userRepository.save(userAshan);
    }

    @Test
    public void updateUseRolesTest(){
        User userRavi = userRepository.findById(2).get();
        Role roleEditor = entityManager.find(Role.class, 3);
        Role roleSalesperson = entityManager.find(Role.class, 2);
        userRavi.getRoles().remove(roleEditor);
        userRavi.addRole(roleSalesperson);

        userRepository.save(userRavi);
    }

    @Test
    public void deleteUseTest(){
        userRepository.deleteById(2);
    }

    @Test
    public void getUserByEmailTest(){
        String email = "ravi@gmail.com";
        User user = userRepository.getUserByEmail(email);

        assertThat(user).isNotNull();
    }

    @Test
    public void countByIdTest(){
        User userById = userRepository.getUserById(1);

        assertThat(userById).isNotNull();
    }

    @Test
    public void disableUserTest(){
        userRepository.updateEnabledStatus(1, false);
    }

    @Test
    public void enableUserTest(){
        userRepository.updateEnabledStatus(1, true);
    }

    @Test
    public void firstListPageTest(){
        int pageNumber = 1;
        int pageSize = 4;
        Pageable pageable = PageRequest.of(pageNumber, pageSize);
        Page<User> page = userRepository.findAll(pageable);

        List<User> listUsers = page.getContent();
        listUsers.forEach(user -> System.out.println(user));

        assertThat(listUsers.size()).isEqualTo(pageSize);
    }

    @Test
    public void searchUsersTest(){
        String keyword = "Bruce";
        int pageNumber = 0;
        int pageSize = 4;
        Pageable pageable = PageRequest.of(pageNumber, pageSize);
        Page<User> page = userRepository.findAll(keyword, pageable);

        List<User> listUsers = page.getContent();
        listUsers.forEach(user -> System.out.println(user));

        assertThat(listUsers.size()).isGreaterThan(0);
    }

}
